This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

![](https://media.giphy.com/media/STqk2WExRDlVTB82q9/giphy.gif)

## Getting Started

Node 10+
NPM 6+

`npm install`

`npm start`

## Testing

### Jest Tests
`npm test`

### Cypress Tests
`npm run cy:run`

`npm run cy:open`

## Notes

### General Thoughts
I took the "short" way for this e.g. didn't take on the optional suggestions:
  - adding clientside filters for segments
  - adding additional tags
  - 
My approach was more of a quick and dirty, broad strokes in short time :sprint: during a long lunch break. I stuck with familiar packages, methodologies etc.

### Decisions
- Used draft.js for a text editor. Seemed like the easiest way to iterate to using "entities" which would make the _additional tags_ feature nice.
  - Imagining funcitonality like adding a link to a google doc.
  - Right now the implementation is purely plain text (minus links being set as html)
- Used formik to handle forms
  - Pretty crude implementation (minimal validation, only handling errors inline for one example, sloppy markup)
- Used a mixture of Cypress and Jest
  - Cypress covers some of the User Story Paths
  - Jest is used for snapshots and some basic unit testing
- Used an "api-wrapper" as a rudimentary breaking out of Components, Context API, and Server Interaction (handles generating random IDs etc)

### Misc
- Some of the component design is imho a little sloppy and abstracted in a way to just cut down on lines of code in a file. Wanted to avoid losing momentum by over optimizing, when really it would be the beginning of establishing formal patterns via a library 
- alot of inline ternaries to keep moving (i don't have ternaries tbh but don't love how it's being used inline for presentational logic)

### Broken Images
- images are broken after upload because at this point I'd assume to get a valid remote url to reference the file. Didn't want to try to pass around a preview Blob, which I don't think would represent any real world scenario for this type of implementation.

### Styles
- Using a combination of antd components, styled-components, and tailwind to keep rolling. I used antd cuz i'm most familiar with it, and styled components + tailwind for the flexibility and ease of moving quickly.

- There is some consideration for smaller screensizes but definitely not ready to ship as is

### Navigation
- One thing this project lacks is easy to use breadcrumbs or back links. It's also something that i noticed about the postscript.io product almost immediately
  - if you go into an individual campaign and then click on the nav it doesn't take you back to the list of campaigns etc. I tried to minimally improve on this