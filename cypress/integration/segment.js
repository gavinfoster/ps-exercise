describe('Segment', () => {
  it('creates new segment', () => {
    cy.visit('/segments/create');
    cy.get('[data-cy=input-name]').type('New Segment');
    cy.get('[data-cy=submit').click();
    cy.get('[data-cy=table-segment-name').first().contains('New Segment');
  });
});
