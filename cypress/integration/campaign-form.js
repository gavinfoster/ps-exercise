describe('Create Campaign', () => {
  beforeEach(() => {
    cy.visit('/campaigns/create');
  });

  it('add and preview message text', () => {
    const initialMessage = 'Adding Message ';

    cy.get('[data-cy=editor]').type(initialMessage);

    cy.get('[data-cy=text-template]').should(($el) => {
      expect($el).to.contain(initialMessage);
    });

    cy.get('[data-cy=button-shop-name]').click();

    cy.get('[data-cy=editor]').should(($el) => {
      expect($el).to.contain('Adding Message {shop_name}');
    });

    cy.get('[data-cy=text-template]').should(($el) => {
      expect($el).to.contain('Adding Message gavin-testing-shop');
    });
  });
});
