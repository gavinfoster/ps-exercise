describe('Campaign List', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('renders open and sent table', () => {
    cy.get('[data-cy=table-sent-campaigns]');
    cy.get('[data-cy=table-open-campaigns]');
  });

  it('filters open campaigns', () => {
    cy.get('[data-cy=filter-open]').click();
    cy.get('[data-cy=table-sent-campaigns]').should('not.exist');
  });

  it('filters sent campaigns', () => {
    cy.get('[data-cy=filter-sent]').click();
    cy.get('[data-cy=table-open-campaigns]').should('not.exist');
  });
});
