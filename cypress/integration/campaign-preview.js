describe('Campaign Preview', () => {
  it('navigates to campaign and contains description name. edits campaign name', () => {
    cy.visit('/');
    cy.contains('Fall Decor Updates').click({ force: true });
    cy.get('[data-cy=campaign-description-name]').should(($el) => expect($el).to.contain('Fall Decor Updates'));

    cy.get('[data-cy="button-campaign-edit').click();
    cy.get('[data-cy=name-input]').type(' Added!');
    cy.get('[data-cy=submit]').click();
    cy.get('[data-cy=campaign-description-name]').should(($el) => expect($el).to.contain('Fall Decor Updates Added!'));
  });
});
