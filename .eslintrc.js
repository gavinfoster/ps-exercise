module.exports = {
  env: {
    browser: true,
    es6: true,
    jest: true,
    'cypress/globals': true
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    cy: true
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  plugins: [
    'react',
    'cypress'
  ],
  rules: {
    "react/no-danger": [0],
    "import/prefer-default-export": [0],
    "react/forbid-prop-types": [0],
    "react/jsx-props-no-spreading": [0],
    "camelcase": ["error", {ignoreDestructuring: true}],
    "react/jsx-filename-extension": [0, { "extensions": [".js", ".jsx"] }],
    "jsx-a11y/label-has-associated-control": [ 2, {
      "labelComponents": ["label"],
      "labelAttributes": ["htmlFor"],
      "controlComponents": ["input"]
    }]
  },
};
