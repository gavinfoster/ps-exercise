import React, {
  useReducer, createContext, useContext,
} from 'react';
import PropTypes from 'prop-types';

export const ShopContext = createContext();
export const useShop = () => useContext(ShopContext);

const initialState = {
  shopName: 'gavin-testing-shop',
  shopLink: '<a href="https://gavin-testing-shop.myshopify.com/">https://tiny.ps/bslhr</a>',
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'update':
      return {
        ...state,
        [action.property]: action.value,
      };
    default:
      throw new Error('Unknown campaigns reducer case');
  }
};

export const ShopProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const update = (property, value) => {
    dispatch({
      type: 'update',
      property,
      value,
    });
  };

  return (
    <ShopContext.Provider
      value={{
        ...state,
        update,
      }}
    >
      {children}
    </ShopContext.Provider>
  );
};


ShopProvider.propTypes = {
  children: PropTypes.element.isRequired,
};
