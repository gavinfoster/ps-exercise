import React from 'react';
import PropTypes from 'prop-types';
import EditForm from './EditForm';
import { useCampaigns } from './CampaignsContext';
import { useSegments } from '../Segments/SegmentsContext';
import { formatMedia } from '../../helpers/formatters';

const CampaignEdit = ({ match }) => {
  const { loadingCampaigns, getCampaignById } = useCampaigns();
  const { loadingSegments } = useSegments();

  if (loadingCampaigns || loadingSegments) return <p>loading campaigns...</p>;

  const campaignId = parseInt(match.params.id, 10);
  const campaign = getCampaignById(campaignId);

  return (
    <EditForm
      segmentId={campaign.segment_id}
      campaignId={campaign.id}
      text={campaign.text}
      name={campaign.name}
      media={formatMedia(campaign.media)}
      stats={campaign.stats}
      status={campaign.status}
    />
  );
};

CampaignEdit.propTypes = {
  match: PropTypes.object.isRequired,
};

export default CampaignEdit;
