import React from 'react';
import { withRouter } from 'react-router-dom';
import {
  Input, Select, Tag,
} from 'antd';
import { isNil } from 'ramda';
import Editor from '../Utility/Editor/Editor';
import FileUpload from '../Utility/FileUpload';
import { useSegments } from '../Segments/SegmentsContext';

const { Option } = Select;

const CampaignForm = ({
  values, handleChange, setFieldValue, segments, editMedia, setEditMedia, setEditText, text,
}) => {
  const { getSegmentById } = useSegments();
  return (
    <>
      <div>
        <label className="flex flex-col mb-2" htmlFor="test">
          <span>Name</span>
          <Input
            label="Name"
            name="name"
            type="text"
            placeholder="Campaign Name"
            data-cy="name-input"
            value={values.name}
            onChange={handleChange}
          />
        </label>
      </div>
      <div>
        <label className="flex flex-col mb-2" htmlFor="test">
          <span>Segment</span>
          <div className="flex">
            <Select
              onChange={(value) => setFieldValue('segment', value)}
              value={values.segment}
            >
              {
            segments.map(({ id, name }) => <Option key={id} value={id}>{name}</Option>)
          }
            </Select>
            {
          isNil(values.segment) ? null
            : (
              <Tag className="flex items-center justify-center ml-4" color="blue">
                { `${getSegmentById(values.segment).subscribers_count} Members`}
              </Tag>
            )
          }
          </div>
        </label>
      </div>
      <div>
        <label className="flex flex-col mb-2" htmlFor="name">
          <span>Media</span>
          <FileUpload
            initialValue={editMedia}
            onChange={setEditMedia}
          />
        </label>
      </div>
      <div>
        <label className="flex flex-col mb-2" htmlFor="name">
          <span>Text</span>
          <Editor
            initialValue={text}
            onChange={(value) => {
              setFieldValue('text', value);
              setEditText(value);
            }}
          />
        </label>
      </div>
    </>
  );
};


export default withRouter(CampaignForm);
