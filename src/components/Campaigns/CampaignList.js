import React, { useState } from 'react';
import { isEmpty } from 'ramda';
import { Button } from 'antd';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { useCampaigns } from './CampaignsContext';
import CampaignTable from './CampaignTable';

const { Group } = Button;

const Header = styled.header.attrs({
  className: 'flex items-center justify-between mb-8',
})``;

const HeaderTitle = styled.h3.attrs({
  className: 'text-2xl font-bold',
})``;

const TableHeader = styled.h4.attrs({
  className: 'text-base font-bold',
})``;

const CreateButton = styled(Button).attrs({
  className: 'mr-4',
  type: 'primary',
})``;

const Campaigns = ({ match }) => {
  // all, open, sent
  const [campaignFilter, setCampaignFilter] = useState('all');
  const { loadingCampaigns, campaigns } = useCampaigns();

  if (loadingCampaigns) return <p>loading campaigns...</p>;

  if (isEmpty(campaigns)) return <p>create new</p>;

  const sentCampaigns = campaigns.filter(({ status }) => status === 'Sent');
  const openCampaigns = campaigns.filter(({ status }) => status === 'Preview');

  return (
    <>
      <Header>
        <HeaderTitle>Campaigns</HeaderTitle>
        <div>
          <Link to="/campaigns/create">
            <CreateButton icon="plus">Create Campaign</CreateButton>
          </Link>
          <Group>
            <Button onClick={() => setCampaignFilter('all')}>All</Button>
            <Button data-cy="filter-open" onClick={() => setCampaignFilter('open')}>Open</Button>
            <Button data-cy="filter-sent" onClick={() => setCampaignFilter('sent')}>Sent</Button>
          </Group>
        </div>
      </Header>
      {
        campaignFilter !== 'sent'
          ? (
            <>
              <TableHeader>Open Campaigns</TableHeader>
              <CampaignTable data-cy="table-open-campaigns" showEdit data={openCampaigns} tagColor="blue" />
            </>
          )
          : null
      }
      {
        campaignFilter !== 'open'
          ? (
            <>
              <TableHeader>Sent Campaigns</TableHeader>
              <CampaignTable data-cy="table-sent-campaigns" includeStats data={sentCampaigns} tagColor="green" />
            </>
          )
          : null
      }
    </>
  );
};

export default Campaigns;
