import React from 'react';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Table, Tag, Button } from 'antd';

const CampaignTable = ({
  data, tagColor, includeStats, history, showEdit, ...props
}) => {
  const handleEdit = (e, recordId) => {
    e.stopPropagation();
    history.push(`/campaigns/${recordId}/edit`);
  };

  const handleDelete = (e) => {
    e.stopPropagation();
    alert('placeholder delete');
  };

  const handleOnRow = (record) => ({
    onClick() {
      history.push(`/campaigns/${record.id}`);
    },
  });

  const columns = [
    {
      title: 'Campaign',
      dataIndex: 'name',
      key: 'name',
      fixed: 'left',
      render: (name) => <h4 data-cy="campaign-row-name" data-name={name}>{name}</h4>,
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: (status) => (
        <Tag color={tagColor}>{status}</Tag>
      ),
    },
    ...(
      includeStats
        ? [{
          title: 'Sent',
          dataIndex: 'stats',
          key: 'sent',
          sorter: (a, b) => a.stats.sent - b.stats.sent,
          render: (set, record) => <div>{record.stats.sent}</div>,
        }] : []),
    ...(
      includeStats
        ? [{
          title: 'Clicked',
          dataIndex: 'stats',
          key: 'clicked',
          sorter: (a, b) => a.stats.clicked - b.stats.clicked,
          render: (clicked, record) => <div>{record.stats.clicked}</div>,
        }] : []),
    {
      title: 'Action',
      key: 'action',
      align: 'right',
      fixed: 'right',
      render: (action, record) => (
        <div
          onClick={(e) => handleEdit(e, record.id)}
          className="text-right relative z-10"
        >
          {
            showEdit ? (
              <Button
                className="mr-2"
                type="primary"
                icon="edit"
                ghost
              >
                Edit
              </Button>
            ) : null
          }
          <Button
            onClick={handleDelete}
            type="danger"
            icon="delete"
            ghost
          />
        </div>
      ),
    },
  ];

  return <Table {...props} onRow={handleOnRow} columns={columns} dataSource={data} />;
};

CampaignTable.propTypes = {
  history: PropTypes.object.isRequired,
  data: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    stats: PropTypes.shape({
      sent: PropTypes.number,
      clicked: PropTypes.number,
    }),
  })).isRequired,
  tagColor: PropTypes.string,
  showEdit: PropTypes.bool,
  includeStats: PropTypes.bool,
};

CampaignTable.defaultProps = {
  tagColor: 'blue',
  showEdit: false,
  includeStats: false,
};

export default withRouter(CampaignTable);
