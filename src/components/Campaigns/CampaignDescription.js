import React from 'react';
import { Descriptions, Tag } from 'antd';
import styled from 'styled-components';
import StatsGraph from './StatsGraph';
import ThumbnailPreview from '../Utility/ThumbnailPreview';

const StatsWrapper = styled.div`
  height: 300px;
`;

const CampaignDescription = ({
  name, text, stats, segmentName, segmentSubscribersCount, media, title,
}) => (
  <Descriptions
    title={title}
    column={{
      xxl: 4, xl: 3, lg: 2, sm: 1,
    }}
    bordered
  >
    <Descriptions.Item span={2} label="Name">
      <span data-cy="campaign-description-name">{name}</span>
    </Descriptions.Item>
    <Descriptions.Item span={2} label="Segment">
      <div className="flex items-center">
        <p>{segmentName}</p>
        <Tag className="ml-1">{segmentSubscribersCount}</Tag>
      </div>
    </Descriptions.Item>
    {
      media ? (
        <Descriptions.Item span={3} label="Media">
          <div className="flex items-center">
            <ThumbnailPreview preview={media} name={media} width={50} height={50} />
          </div>
        </Descriptions.Item>
      ) : null
    }
    <Descriptions.Item span={3} label="Text">{text}</Descriptions.Item>
    {
        stats ? (
          <Descriptions.Item label="Stats" span={1}>
            <StatsWrapper>
              <StatsGraph sent={stats.sent} clicked={stats.clicked} />
            </StatsWrapper>
          </Descriptions.Item>
        ) : null
      }
  </Descriptions>
);

export default CampaignDescription;
