import React from 'react';
import { withRouter } from 'react-router-dom';
import { Button, Tag } from 'antd';
import PropTypes from 'prop-types';
import { isNil } from 'ramda';
import styled from 'styled-components';
import Preview from '../Preview/Preview';
import { useCampaigns } from './CampaignsContext';
import { useSegments } from '../Segments/SegmentsContext';
import CampaignDescription from './CampaignDescription';

const Wrapper = styled.div.attrs({
  className: 'grid grid-cols-12',
})``;

const Left = styled.div.attrs({
  className: 'col-span-12 md:col-span-8',
})``;

const Right = styled.div.attrs({
  className: 'col-span-12 md:col-span-4 flex items-center justify-center',
})``;

const Footer = styled.footer.attrs({
  className: 'col-span-12 flex mt-4',
})``;

const CampaignSingle = ({
  history,
  match,
}) => {
  const { loadingCampaigns, getCampaignById } = useCampaigns();
  const { loadingSegments, getSegmentById } = useSegments();

  if (loadingCampaigns || loadingSegments) return <p>loading campaigns...</p>;

  const campaignId = parseInt(match.params.id, 10);
  const campaign = getCampaignById(campaignId);

  // TODO: can't find campaign
  if (isNil(campaign)) return <p>Wooops, can't find this campaign.</p>;

  const {
    segment_id, status, stats, text, name, media,
  } = campaign;
  const segment = getSegmentById(segment_id);

  return (
    <Wrapper>
      <Left>
        <CampaignDescription
          name={name}
          text={text}
          stats={stats}
          segmentName={segment.name}
          segmentSubscribersCount={segment.subscribers_count}
          media={media}
          title={(
            <header className="flex items-center">
              <h4>{`${name} Campaign`}</h4>
              <Tag className="ml-1" color="blue">{status}</Tag>
            </header>
              )}
        />
      </Left>
      <Right>
        <Preview
          text={text}
          media={{
            preview: media,
            name: media,
          }}
        />
      </Right>
      {
        status === 'Preview' ? (
          <Footer>
            <Button
              type="primary"
              icon="edit"
              data-cy="button-campaign-edit"
              onClick={() => history.push(`/campaigns/${campaignId}/edit`)}
              className="mr-2 w-full sm:w-auto"
            >
              Edit Campaign
            </Button>
          </Footer>
        ) : null
      }
    </Wrapper>
  );
};

CampaignSingle.propTypes = {
  history: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
};

export default withRouter(CampaignSingle);
