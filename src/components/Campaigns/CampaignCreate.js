import React from 'react';
import styled from 'styled-components';
import CreateForm from './CreateForm';
import { useSegments } from '../Segments/SegmentsContext';

const HeaderTitle = styled.h3.attrs({
  className: 'text-2xl font-bold',
})``;

const CampaignCreate = () => {
  const { loadingSegments } = useSegments();

  if (loadingSegments) return <p>loading campaigns...</p>;

  return (
    <>
      <HeaderTitle>Create Campaign</HeaderTitle>
      <CreateForm />
    </>
  );
};

export default CampaignCreate;
