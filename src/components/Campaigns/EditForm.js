import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import { Formik, Form } from 'formik';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import styled from 'styled-components';
import Preview from '../Preview/Preview';
import ApiWrapper from '../../helpers/api-wrapper';
import { useCampaigns } from './CampaignsContext';
import { useSegments } from '../Segments/SegmentsContext';
import Submit from '../Utility/Submit';
import CampaignForm from './CampaignForm';

const FormWrapper = styled(Form).attrs({
  className: 'grid grid-cols-12 mt-4',
})``;

const Left = styled.div.attrs({
  className: 'col-span-12 md:col-span-8',
})``;

const Right = styled.div.attrs({
  className: 'col-span-12 md:col-span-4 flex items-center justify-center',
})``;

const Footer = styled.footer.attrs({
  className: 'col-span-12 flex mt-4',
})``;

const EditForm = ({
  history,
  segmentId,
  campaignId,
  text,
  name,
  media,

}) => {
  const [editText, setEditText] = useState(text);
  const [editMedia, setEditMedia] = useState(media);
  const { updateCampaign } = useCampaigns();
  const { segments } = useSegments();

  const createCampaign = async (values, { setFieldError, setSubmitting }) => {
    try {
      setSubmitting(true);
      const campaign = await ApiWrapper.updateCampaign({
        ...values,
        text: editText,
        media: editMedia,
      });
      updateCampaign(campaign);
    } catch (err) {
      setFieldError('general', err.message);
    }
    setSubmitting(false);
    history.push(`/campaigns/${campaignId}`);
  };

  return (
    <Formik
      initialValues={{
        id: campaignId,
        name,
        segment: segmentId,
        text,

      }}
      validationSchema={Yup.object({
        name: Yup.string()
          .required('Required'),
        segment: Yup.number()
          .required('Required'),
      })}
      onSubmit={createCampaign}
    >
      {({
        values, handleChange, setFieldValue, handleSubmit, isSubmitting,
      }) => (
        <FormWrapper onSubmit={handleSubmit}>
          <Left>
            <CampaignForm
              values={values}
              handleChange={handleChange}
              setFieldValue={setFieldValue}
              segments={segments}
              editMedia={editMedia}
              setEditMedia={setEditMedia}
              setEditText={setEditText}
              text={editText}
            />
          </Left>
          <Right>
            <Preview
              text={editText}
              media={editMedia}
            />
          </Right>
          <Footer>
            <Submit isSubmitting={isSubmitting} />
          </Footer>
        </FormWrapper>
      )}
    </Formik>
  );
};

EditForm.propTypes = {
  history: PropTypes.object.isRequired,
  campaignId: PropTypes.number,
  segmentId: PropTypes.number,
  text: PropTypes.string,
  name: PropTypes.string,
  media: PropTypes.string,
};

EditForm.defaultProps = {
  campaignId: null,
  segmentId: null,
  text: null,
  name: null,
  media: null,
};


export default withRouter(EditForm);
