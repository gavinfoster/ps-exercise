import React, { useState } from 'react';
import { withRouter } from 'react-router-dom';
import { Formik, Form } from 'formik';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import styled from 'styled-components';
import Preview from '../Preview/Preview';
import ApiWrapper from '../../helpers/api-wrapper';
import { useCampaigns } from './CampaignsContext';
import { useSegments } from '../Segments/SegmentsContext';
import Submit from '../Utility/Submit';
import CampaignForm from './CampaignForm';

const FormWrapper = styled(Form).attrs({
  className: 'grid grid-cols-12 mt-4',
})``;

const Left = styled.div.attrs({
  className: 'col-span-12 md:col-span-8',
})``;

const Right = styled.div.attrs({
  className: 'col-span-12 md:col-span-4 flex items-center justify-center',
})``;

const Footer = styled.footer.attrs({
  className: 'col-span-12 flex mt-4',
})``;

const Create = ({
  history,
}) => {
  const [editText, setEditText] = useState();
  const [editMedia, setEditMedia] = useState();
  const { addCampaign } = useCampaigns();
  const { segments } = useSegments();

  const createCampaign = async (values, { setFieldError, setSubmitting }) => {
    let campaignId;
    try {
      setSubmitting(true);
      const { name, segment } = values;
      const campaign = await ApiWrapper.createCampaign(name, segment, editText, editMedia);
      addCampaign(campaign);
      campaignId = campaign.id;
    } catch (err) {
      setFieldError('general', err.message);
    }
    setSubmitting(false);
    history.push(`/campaigns/${campaignId}`);
  };

  return (
    <Formik
      initialValues={{
        name: '',
        segment: null,
      }}
      validationSchema={Yup.object({
        name: Yup.string()
          .required('Required'),
        segment: Yup.number()
          .required('Required'),
      })}
      onSubmit={createCampaign}
    >
      {({
        values, handleChange, setFieldValue, handleSubmit, isSubmitting,
      }) => (
        <FormWrapper onSubmit={handleSubmit}>
          <Left>
            <CampaignForm
              values={values}
              handleChange={handleChange}
              setFieldValue={setFieldValue}
              segments={segments}
              editMedia={editMedia}
              setEditMedia={setEditMedia}
              setEditText={setEditText}
            />
          </Left>
          <Right>
            <Preview
              text={editText}
              media={editMedia}
            />
          </Right>
          <Footer>
            <Submit isSubmitting={isSubmitting} />
          </Footer>
        </FormWrapper>
      )}
    </Formik>
  );
};

Create.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(Create);
