import React from 'react';
import {
  ResponsiveContainer, PieChart, Pie, Label, text,
} from 'recharts';

const CampaignDescription = ({
  sent, clicked,
}) => {
  const outerData = [
    {
      name: `${clicked} Clicked`,
      value: clicked,
    },
    {
      name: `${sent} Sent`,
      value: sent - clicked,
      fill: '#434190',
    },
  ];

  return (
    <ResponsiveContainer height="100%" width="100%">
      <PieChart>
        <Pie
          data={outerData}
          dataKey="value"
          nameKey="name"
          cx="50%"
          cy="50%"
          innerRadius={60}
          fill="#667eea"
          label={({ name }) => name}
        />
      </PieChart>
    </ResponsiveContainer>
  );
};

export default CampaignDescription;
