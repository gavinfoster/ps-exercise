import React, {
  useReducer, useEffect, createContext, useContext,
} from 'react';
import { isNil, isEmpty } from 'ramda';
import PropTypes from 'prop-types';
import { notification } from 'antd';
import ApiWrapper from '../../helpers/api-wrapper';

export const CampaignsContext = createContext();
export const useCampaigns = () => useContext(CampaignsContext);

const initialState = {
  loadingCampaigns: true,
  campaigns: null,
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'set':
      return { ...state, campaigns: action.campaigns };
    case 'loading':
      return { ...state, loadingCampaigns: action.loading };
    case 'add':
      return { ...state, campaigns: [action.campaign, ...state.campaigns] };
    case 'update':

      // NOTE: re-inserting to the top since to represented most recently updated :shrug:
      return {
        ...state,
        campaigns: [
          {
            ...state.campaigns.find(({ id }) => id === action.campaign.id),
            ...action.campaign,
          },
          ...state.campaigns.filter((campaign) => campaign.id !== action.campaign.id)],
      };
    case 'remove':
      return { ...state, campaigns: state.campaigns.filter(({ id }) => id !== action.id) };
    default:
      throw new Error('Unknown campaigns reducer case');
  }
};

export const CampaignsProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const setLoading = (loading) => {
    dispatch({
      type: 'loading',
      loading,
    });
  };

  const setCampaigns = (campaigns) => {
    dispatch({
      type: 'set',
      campaigns,
    });
  };

  const addCampaign = (campaign) => {
    dispatch({
      type: 'add',
      campaign,
    });
  };

  const updateCampaign = (campaign) => {
    dispatch({
      type: 'update',
      campaign,
    });
  };

  const removeCampaign = (id) => {
    dispatch({
      type: 'remove',
      id,
    });
  };

  const getCampaignById = (id) => {
    if (isNil(state.campaigns) || isEmpty(state.campaigns)) return null;

    return state.campaigns.find((campaign) => campaign.id === id);
  };

  const searchCampaigns = async () => {
    setLoading(true);
    try {
      const campaigns = await ApiWrapper.getCampaigns();
      setCampaigns(campaigns);
    } catch (err) {
      notification.error({ message: err.message });
    }
    setLoading(false);
  };

  useEffect(() => {
    searchCampaigns();
    // eslint-disable-next-line
  }, []);

  return (
    <CampaignsContext.Provider
      value={{
        ...state,
        setCampaigns,
        addCampaign,
        updateCampaign,
        removeCampaign,
        searchCampaigns,
        getCampaignById,
      }}
    >
      {children}
    </CampaignsContext.Provider>
  );
};


CampaignsProvider.propTypes = {
  children: PropTypes.element.isRequired,
};
