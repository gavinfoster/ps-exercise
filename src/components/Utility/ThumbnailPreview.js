import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const ThumbImage = styled.img.attrs({
  className: 'w-full object-contain object-center',
})``;

const ThumbName = styled.p.attrs({
  className: 'ml-2 mr-2 truncate',
})``;

const ThumbnailPreview = ({
  name, preview, width, height,
}) => {
  const ThumbContainer = styled.div.attrs({
    className: 'flex shadow',
  })`
    min-width: ${width}px;
    max-width: ${width}px;
    max-height: ${height}px;
  `;
  return (
    <>
      <ThumbContainer>
        <ThumbImage
          src={preview}
        />
      </ThumbContainer>
      <ThumbName>{name}</ThumbName>
    </>
  );
};

ThumbnailPreview.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  name: PropTypes.string,
  preview: PropTypes.string,
};

ThumbnailPreview.defaultProps = {
  width: 100,
  height: 100,
  name: null,
  preview: null,
};

export default ThumbnailPreview;
