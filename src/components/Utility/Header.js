import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const HeaderTitle = styled.h3.attrs({
  className: 'text-2xl font-bold',
})``;

const Header = ({ children }) => (
  <HeaderTitle>
    {children}
  </HeaderTitle>
);

Header.propTypes = {
  children: PropTypes.element.isRequired,
};

export default Header;
