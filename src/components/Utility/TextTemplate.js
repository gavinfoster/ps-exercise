import React from 'react';
import PropTypes from 'prop-types';
import { isNil } from 'ramda';
import { useShop } from '../Shop/ShopContext';
import { renderTextTemplate } from '../../helpers/formatters';

const DEFAULT_NAME = 'Elizabeth';

const TextTemplate = ({ templateString, ...props }) => {
  const { shopName, shopLink } = useShop();

  if (isNil(templateString)) return null;

  const templateMap = {
    '{shop_name}': shopName,
    '{shop_link}': shopLink,
    '{first_name}': DEFAULT_NAME,
  };

  const renderedText = renderTextTemplate(templateMap, templateString);

  return (
    <p data-cy="text-template" {...props} className="break-all" dangerouslySetInnerHTML={{ __html: renderedText }} />
  );
};

TextTemplate.propTypes = {
  templateString: PropTypes.string.isRequired,
};

export default TextTemplate;
