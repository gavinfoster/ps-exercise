import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Button, Icon, Spin } from 'antd';

const BaseButton = styled(Button).attrs({
  className: 'flex justify-center w-full sm:w-1/4',
  type: 'primary',
  htmlType: 'submit',
  'data-cy': 'submit',
})``;

const SubmitButton = styled(BaseButton).attrs({
  icon: 'upload',
})``;


const Submit = ({ isSubmitting, ...props }) => {
  if (!isSubmitting) return <SubmitButton>Submit</SubmitButton>;

  const SpinIcon = <Icon type="loading" style={{ fontSize: 24, color: '#fff' }} spin />;

  return (
    <BaseButton {...props}>
      <Spin indicator={SpinIcon} spin />
    </BaseButton>
  );
};

Submit.propTypes = {
  isSubmitting: PropTypes.bool,
};

Submit.defaultProps = {
  isSubmitting: false,
};

export default Submit;
