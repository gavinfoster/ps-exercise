import React from 'react';
import { ErrorMessage } from 'formik';
import styled from 'styled-components';

const Error = styled.div.attrs({
  className: 'text-red-600',
})`
  font-size:.65rem;
`;

const InlineError = (props) => <ErrorMessage component={Error} {...props} />;

export default InlineError;
