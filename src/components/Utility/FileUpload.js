/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { useState, useCallback } from 'react';
import { useDropzone } from 'react-dropzone';
import PropTypes from 'prop-types';
import { isNil } from 'ramda';
import styled from 'styled-components';
import { Button } from 'antd';
import ThumbnailPreview from './ThumbnailPreview';

const DropContainer = styled.section.attrs({
  className: 'flex items-center justify-center bg-gray-100 p-2',
})`
  border: 2px dashed #eee;
`;

const PreviewContainer = styled.aside.attrs({
  className: 'flex items-center justify-between w-full',
})``;

const FileUpload = ({ initialValue, onChange }) => {
  const [file, setFile] = useState(initialValue);

  const updateFile = (fileUpdate) => {
    setFile(fileUpdate);
    onChange(fileUpdate);
  };

  const onDrop = useCallback((acceptedFiles) => {
    const acceptedFile = Object.assign(acceptedFiles[0], {
      preview: URL.createObjectURL(acceptedFiles[0]),
    });
    updateFile(acceptedFile);
  }, []);

  const { getRootProps, getInputProps, open } = useDropzone({
    accept: 'image/*',
    onDrop,
    noClick: true,
    noKeyboard: true,
  });

  return (
    <>
      <DropContainer {...getRootProps()}>
        <input {...getInputProps()} />
        {
          isNil(file)
            ? (
              <p
                className="p-4"
                onClick={open}
              >
                Drag 'n' drop some an image or gif here, or click to select files
              </p>
            )
            : (
              <PreviewContainer>
                <ThumbnailPreview width={50} height={50} preview={file.preview} name={file.name} />
                <Button onClick={() => updateFile(null)} className="ml-auto mr-2" type="danger">Remove</Button>
                <Button onClick={open}>Overwrite Image</Button>
              </PreviewContainer>
            )
        }
      </DropContainer>
    </>
  );
};

FileUpload.propTypes = {
  initialValue: PropTypes.string,
  onChange: PropTypes.func,
};

FileUpload.defaultProps = {
  initialValue: null,
  onChange: () => {},
};

export default FileUpload;
