import React, { useRef, useEffect } from 'react';
import {
  Editor, EditorState, ContentState, Modifier,
} from 'draft-js';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Header from './Header';

const EditorWrapper = styled.div.attrs({
  className: 'cursor-text p-4 rounded-sm rounded-t-none',
})`
  border: 1px solid #d9d9d9;
  min-height: 200px;
`;

const EditorComponent = ({ initialValue, onChange }) => {
  const initialState = EditorState.createWithContent(ContentState.createFromText(initialValue));
  const [editorState, setEditorState] = React.useState(initialState);
  const editorRef = useRef(null);

  const addText = (text) => {
    const selectionState = editorState.getSelection();
    const contentState = editorState.getCurrentContent();

    const newContent = Modifier.replaceText(
      contentState,
      selectionState,
      text,
    );

    const newEditorState = EditorState.push(editorState, newContent, 'insert-characters');

    setEditorState(newEditorState);
  };

  useEffect(() => {
    onChange(editorState.getCurrentContent().getPlainText());
  }, [editorState]);

  return (
    <>
      <Header addText={addText} />
      <EditorWrapper
        data-cy="editor"
        onClick={() => editorRef.current.focus()}
      >
        <Editor
          ref={editorRef}
          editorState={editorState}
          onChange={setEditorState}
        />
      </EditorWrapper>
    </>
  );
};

EditorComponent.propTypes = {
  initialValue: PropTypes.string,
  onChange: PropTypes.func,
};

EditorComponent.defaultProps = {
  initialValue: '',
  onChange: () => {},
};

export default EditorComponent;
