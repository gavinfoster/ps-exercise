import React from 'react';
import { Button } from 'antd';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Wrapper = styled.div.attrs({
  className: 'grid bg-white',
})`
  grid-template-areas:
    'header header header'
    'col-1 col-2 col-3';

  padding:1px;
  gap:1px;
  border: 1px solid #ccc;
  border-bottom: none;
`;

const GridLabel = styled.p.attrs({
  className: 'text-center text-gray-600',
})`
  background-color:#f4f4f4;
  font-size: .65rem;
  grid-area: header;
`;
const GridButton = styled(Button).attrs({
  className: 'rounded-none text-gray-700 text-xs',
  size: 'small',
})`
  background-color:#f4f4f4;
`;

const EditorHeader = ({ addText }) => (
  <Wrapper>
    <GridLabel>Tags</GridLabel>
    <GridButton
      data-cy="button-shop-link"
      style={{ gridArea: 'col-1' }}
      type="link"
      onClick={() => addText('{shop_link}')}
    >
      Shop Link
    </GridButton>
    <GridButton
      data-cy="button-first-name"
      style={{ gridArea: 'col-2' }}
      type="link"
      onClick={() => addText('{first_name}')}
    >
      First Name
    </GridButton>
    <GridButton
      data-cy="button-shop-name"
      style={{ gridArea: 'col-3' }}
      type="link"
      onClick={() => addText('{shop_name}')}
    >
      Shop Name
    </GridButton>
  </Wrapper>
);

EditorHeader.propTypes = {
  addText: PropTypes.func.isRequired,
};

export default EditorHeader;
