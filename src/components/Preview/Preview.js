import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { isNil, isEmpty } from 'ramda';
import TextTemplate from '../Utility/TextTemplate';

const PhoneWrapper = styled.img.attrs({
  src: 'https://app.postscript.io/iphone_outline_new.ec194b62.png',
})``;

const ContentWrapper = styled.div.attrs({
  className: 'absolute top-0 left-0 w-full h-full p-6 pt-12',
})``;

const Message = styled.div.attrs({
  className: 'rounded bg-gray-400 w-4/5 p-1 text-xs',
})``;

const Media = styled.img.attrs({
  className: 'rounded w-3/5 mb-2',
})``;

const Preview = ({ text, media }) => (
  <div className="relative inline-block">
    <PhoneWrapper />
    <ContentWrapper>
      {
          isNil(media) ? null : <Media src={media.preview} />
        }
      {
          isEmpty(text) ? null : (
            <Message>
              <TextTemplate templateString={text} />
            </Message>
          )
        }
    </ContentWrapper>
  </div>
);
Preview.propTypes = {
  text: PropTypes.string,
  media: PropTypes.string,
};

Preview.defaultProps = {
  media: null,
  text: '',
};

export default Preview;
