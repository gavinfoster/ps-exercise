import React, { useState } from 'react';
import { Layout } from 'antd';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Logo from './Logo';
import Footer from './Footer';
import Header from './Header';
import Content from './Content';
import SidebarMenu from './SidebarMenu';

const { Sider } = Layout;

const Wrapper = styled(Layout)`
  min-height: 100vh;
`;

const LayoutContainer = ({ children }) => {
  const [isCollapsed, setIsCollapsed] = useState(false);

  return (
    <Wrapper>
      <Sider
        collapsible
        collapsed={isCollapsed}
        onCollapse={setIsCollapsed}
        breakpoint="lg"
      >
        <Logo isCollapsed={isCollapsed} />
        <SidebarMenu />
      </Sider>
      <Layout>
        <Header />
        <Content>
          {children}
        </Content>
        <Footer />
      </Layout>
    </Wrapper>
  );
};

LayoutContainer.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  children: PropTypes.array.isRequired,
};

export default LayoutContainer;
