import React from 'react';
import { Layout } from 'antd';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const { Content } = Layout;

const ContentContainer = styled(Content).attrs({
  className: 'm-4',
})``;

const ContentMain = styled.main.attrs({
  className: 'p-4 bg-white',
})`
  min-height: 360px;
`;

const ContentComponent = ({ children }) => (
  <ContentContainer>
    <ContentMain>
      {children}
    </ContentMain>
  </ContentContainer>
);

ContentComponent.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  children: PropTypes.array.isRequired,
};

export default ContentComponent;
