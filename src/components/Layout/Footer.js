import React from 'react';
import { Layout } from 'antd';

const { Footer } = Layout;

const FooterComponent = () => (
  <Footer style={{ textAlign: 'center' }}>
    Postscript Exercise
  </Footer>
);

export default FooterComponent;
