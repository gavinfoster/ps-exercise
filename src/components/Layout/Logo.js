import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Logo = styled.h1.attrs({
  className: 'font-extrabold text-center text-lg p-4 text-white',
})``;

const LayoutComponent = ({ isCollapsed }) => (isCollapsed
  ? <Logo>pse</Logo>
  : <Logo>postscript exercise</Logo>);

LayoutComponent.propTypes = {
  isCollapsed: PropTypes.bool,
};

LayoutComponent.defaultProps = {
  isCollapsed: false,
};

export default LayoutComponent;
