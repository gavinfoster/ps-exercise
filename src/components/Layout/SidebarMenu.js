import React from 'react';
import { Menu, Icon } from 'antd';
import { Link } from 'react-router-dom';

const SidebarMenu = () => (
  <Menu theme="dark" mode="inline" defaultSelectedKeys={['campaigns']}>
    <Menu.Item key="campaigns">
      <Link to="/campaigns">
        <Icon type="mail" />
        <span>Campaigns</span>
      </Link>
    </Menu.Item>
    <Menu.Item key="segments">
      <Link to="/segments">
        <Icon type="cluster" />
        <span>Segments</span>
      </Link>
    </Menu.Item>
  </Menu>
);

export default SidebarMenu;
