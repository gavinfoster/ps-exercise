import React from 'react';
import {
  Layout, Avatar, Dropdown, Menu,
} from 'antd';
import styled from 'styled-components';

const { Header } = Layout;

const HeaderContainer = styled(Header).attrs({
  className: 'pl-4 pr-4 bg-white flex justify-end items-center',
})`
  height: 44px;
  line-height: 44px;
`;

const UserHeader = styled.div.attrs({
  className: 'cursor-pointer flex items-center',
})``;

const UserAvatar = styled(Avatar).attrs({
  className: 'bg-indigo-700 mr-2',
  icon: 'user',
})`
  height: 30px;
  width: 30px;
`;

const Username = styled.p.attrs({
  className: 'text-sm font-semibold text-gray-600',
})``;

const userMenu = (
  <Menu>
    <Menu.Item>Settings</Menu.Item>
    <Menu.Item>Logout</Menu.Item>
  </Menu>
);

const HeaderComponent = () => (
  <HeaderContainer>
    <Dropdown overlay={userMenu} placement="bottomRight">
      <UserHeader>
        <UserAvatar />
        <Username>Username</Username>
      </UserHeader>
    </Dropdown>
  </HeaderContainer>
);

export default HeaderComponent;
