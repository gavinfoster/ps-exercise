import React from 'react';
import { withRouter } from 'react-router-dom';
import {
  Formik, Form, Field, ErrorMessage,
} from 'formik';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import styled from 'styled-components';
import ApiWrapper from '../../helpers/api-wrapper';
import { useSegments } from './SegmentsContext';
import Submit from '../Utility/Submit';
import InlineError from '../Utility/ErrorInline';

const FormWrapper = styled(Form).attrs({
  className: 'flex flex-col mt-4',
})``;

const HeaderTitle = styled.h3.attrs({
  className: 'text-2xl font-bold',
})``;

// TODO: fix hacky implementation once spending time on forms (borrowing antd styles)
const InputField = styled(Field)`
  width: 100%;
  height: 32px;
  padding: 4px 11px;
  color: rgba(0, 0, 0, 0.65);
  font-size: 14px;
  line-height: 1.5;
  border: 1px solid #d9d9d9;
  border-radius: 4px;
`;

const SegmentCreate = ({
  history,
}) => {
  const { addSegment } = useSegments();

  const createSegment = async (values, { setFieldError, setSubmitting }) => {
    try {
      setSubmitting(true);
      const segment = await ApiWrapper.createSegment(values.name);
      addSegment(segment);
    } catch (err) {
      setFieldError('general', err.message);
    }
    setSubmitting(false);
    history.push('/segments');
  };

  return (
    <>
      <HeaderTitle>Create Segment</HeaderTitle>
      <Formik
        initialValues={{
          name: '',
        }}
        validationSchema={Yup.object({
          name: Yup.string()
            .min(2, 'Too Short')
            .required('Required'),
        })}
        onSubmit={createSegment}
      >
        {({
          isSubmitting,
        }) => (
          <FormWrapper>
            <div>
              <label className="flex flex-col mb-2" htmlFor="test">
                <header className="flex items-center">
                  <span>Name</span>
                  <InlineError className="ml-4" name="name" />
                </header>
                <InputField
                  data-cy="input-name"
                  placeholder="Segment Name"
                  name="name"
                />
              </label>
            </div>
            <Submit isSubmitting={isSubmitting} />
          </FormWrapper>
        )}
      </Formik>
    </>
  );
};

SegmentCreate.propTypes = {
  history: PropTypes.object.isRequired,
};

export default withRouter(SegmentCreate);
