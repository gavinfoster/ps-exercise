import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

const SegmentTable = ({
  data,
}) => {
  const columns = [
    {
      title: 'Segment',
      dataIndex: 'name',
      key: 'name',
      fixed: 'left',
      render: (name) => <span data-cy="table-segment-name">{name}</span>,
    },
    {
      title: 'Subscribers',
      dataIndex: 'subscribers_count',
      key: 'subscribers_count',
      sorter: (a, b) => a.subscribers_count - b.subscribers_count,
    },
  ];

  return <Table columns={columns} dataSource={data} />;
};

SegmentTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    status: PropTypes.string.isRequired,
    stats: PropTypes.shape({
      sent: PropTypes.number,
      clicked: PropTypes.number,
    }),
  })).isRequired,
};

export default SegmentTable;
