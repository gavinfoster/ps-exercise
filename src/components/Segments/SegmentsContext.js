import React, {
  useReducer, useEffect, createContext, useContext,
} from 'react';
import { isEmpty, isNil } from 'ramda';
import PropTypes from 'prop-types';
import { notification } from 'antd';
import ApiWrapper from '../../helpers/api-wrapper';

export const SegmentsContext = createContext();
export const useSegments = () => useContext(SegmentsContext);

const initialState = {
  loadingSegments: true,
  segments: [],
};

const reducer = (state, action) => {
  switch (action.type) {
    case 'set':
      return { ...state, segments: action.segments };
    case 'loading':
      return { ...state, loadingSegments: action.loading };
    case 'add':
      return { ...state, segments: [action.segment, ...state.segments] };
    default:
      throw new Error('Unknown segments reducer case');
  }
};

export const SegmentsProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const setLoading = (loading) => {
    dispatch({
      type: 'loading',
      loading,
    });
  };

  const setSegments = (segments) => {
    dispatch({
      type: 'set',
      segments,
    });
  };

  const addSegment = (segment) => {
    dispatch({
      type: 'add',
      segment,
    });
  };

  const getSegmentById = (id) => {
    if (isNil(state.segments) || isEmpty(state.segments)) return null;

    return state.segments.find((segment) => segment.id === id);
  };

  const searchSegments = async () => {
    setLoading(true);
    try {
      const segments = await ApiWrapper.getSegments();

      setSegments(segments);
    } catch (err) {
      notification.error({ message: err.message });
    }
    setLoading(false);
  };

  useEffect(() => {
    searchSegments();
    // eslint-disable-next-line
  }, []);

  return (
    <SegmentsContext.Provider
      value={{
        ...state,
        setSegments,
        addSegment,
        searchSegments,
        getSegmentById,
      }}
    >
      {children}
    </SegmentsContext.Provider>
  );
};


SegmentsProvider.propTypes = {
  children: PropTypes.element.isRequired,
};
