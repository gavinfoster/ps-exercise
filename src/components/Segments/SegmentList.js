import React from 'react';
import { isEmpty } from 'ramda';
import { Button } from 'antd';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import SegmentTable from './SegmentTable';
import { useSegments } from './SegmentsContext';

const Header = styled.header.attrs({
  className: 'flex items-center justify-between mb-8',
})``;

const HeaderTitle = styled.h3.attrs({
  className: 'text-2xl font-bold',
})``;

const CreateButton = styled(Button).attrs({
  className: 'mr-4',
  type: 'primary',
})``;

const SegmentList = () => {
  const { loadingCampaigns, segments } = useSegments();

  if (loadingCampaigns) return <p>loading campaigns...</p>;

  if (isEmpty(segments)) return <p>create new</p>;

  return (
    <>
      <Header>
        <HeaderTitle>Segments</HeaderTitle>
        <div>
          <Link to="/segments/create">
            <CreateButton icon="plus">Create Segment</CreateButton>
          </Link>
        </div>
      </Header>
      <>
        <SegmentTable data={segments} />
      </>
    </>
  );
};

export default SegmentList;
