import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('matches snapshot', () => {
  const appComponent = render(<App />);
  expect(appComponent).toMatchSnapshot();
});
