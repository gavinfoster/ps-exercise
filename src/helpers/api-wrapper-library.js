/* eslint-disable camelcase */
/* eslint-disable class-methods-use-this */
import data from '../data/data.json';
import { formatFileToMedia } from './formatters';

const mockDelay = (timeout = 1000) => new Promise((resolve) => setTimeout(resolve, timeout));

const generateSemiRandomInteger = (max = 10000) => Math.round(Math.random() * max);

class ApiWrapper {
  constructor() {
    this.data = data;
  }

  async getCampaigns() {
    await mockDelay();
    return this.data.campaigns;
  }

  async createCampaign(name, segmentId, text, media) {
    await mockDelay();

    return {
      id: generateSemiRandomInteger(),
      status: 'Preview',
      name,
      segment_id: segmentId,
      text,
      media: formatFileToMedia(media),
      stats: null,
    };
  }

  async updateCampaign(updatedCampaign) {
    await mockDelay();
    return { ...updatedCampaign, media: formatFileToMedia(updatedCampaign.media) };
  }

  async getSegments() {
    await mockDelay();
    return this.data.segments;
  }

  async createSegment(name) {
    await mockDelay();

    return {
      id: generateSemiRandomInteger(),
      subscribers_count: generateSemiRandomInteger(1000),
      name,
    };
  }
}

export default ApiWrapper;
