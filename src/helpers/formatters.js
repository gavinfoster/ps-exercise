import {
  isNil, is, has, hasIn,
} from 'ramda';

// eslint-disable-next-line arrow-body-style
export const renderTextTemplate = (templateMap, templateString) => {
  return Object.keys(templateMap)
    .reduce((text, interpolationKey) => text.replace(new RegExp(interpolationKey, 'g'), templateMap[interpolationKey]), templateString);
};

export const formatMedia = (media) => {
  if (isNil(media)) return null;

  if (is(String, media)) {
    return {
      preview: media,
      name: media,
    };
  }

  return media;
};

export const formatFileToMedia = (file) => {
  if (isNil(file)) return null;

  if (hasIn('name', file)) return file.name;

  return file;
};
