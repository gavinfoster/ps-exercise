import { renderTextTemplate, formatMedia, formatFileToMedia } from './formatters';

const formatMap = {
  '{shop_name}': 'The Big Shop',
  '{first_name}': 'Katherine',
};

const templateText = 'Hey there {first_name}, come visit {shop_name}. Come to {shop_name}';
const formattedText = 'Hey there Katherine, come visit The Big Shop. Come to The Big Shop';
const imageAddress = 'http://image.jpg';

test('tests simple template render', () => {
  expect(renderTextTemplate(formatMap, templateText)).toBe(formattedText);
});

test('format media (null)', () => {
  expect(formatMedia(null)).toBe(null);
});

test('format media (string)', () => {
  expect(formatMedia('http://image.jpg')).toStrictEqual({
    preview: imageAddress,
    name: imageAddress,
  });
});

test('format media (null)', () => {
  expect(formatFileToMedia(null)).toBe(null);
});

test('format media (File)', () => {
  expect(formatFileToMedia({ name: 'dogs.jpg' })).toBe('dogs.jpg');
});
