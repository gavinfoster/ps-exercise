import ApiWrapperContructor from './api-wrapper-library';

const ApiWrapper = new ApiWrapperContructor();

export default ApiWrapper;
