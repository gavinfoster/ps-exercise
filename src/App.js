import React from 'react';
import {
  BrowserRouter as Router, Route, Switch, Redirect,
} from 'react-router-dom';
import './styles/index.css';
import Layout from './components/Layout/Container';
import CampaignCreate from './components/Campaigns/CampaignCreate';
import CampaignList from './components/Campaigns/CampaignList';
import CampaignEdit from './components/Campaigns/CampaignEdit';
import CampaignSingle from './components/Campaigns/CampaignSingle';
import SegmentList from './components/Segments/SegmentList';
import SegmentCreate from './components/Segments/SegmentCreate';
import { SegmentsProvider } from './components/Segments/SegmentsContext';
import { CampaignsProvider } from './components/Campaigns/CampaignsContext';
import { ShopProvider } from './components/Shop/ShopContext';

const App = () => (
  <ShopProvider>
    <SegmentsProvider>
      <CampaignsProvider>
        <Router>
          <Layout>
            <Switch>
              <Route
                exact
                path="/campaigns"
                component={CampaignList}
              />
              <Route
                exact
                path="/campaigns/create"
                component={CampaignCreate}
              />
              <Route
                exact
                path="/campaigns/:id/edit"
                component={CampaignEdit}
              />
              <Route
                exact
                path="/campaigns/:id"
                component={CampaignSingle}
              />
              <Route
                exact
                path="/segments"
                component={SegmentList}
              />
              <Route
                exact
                path="/segments/create"
                component={SegmentCreate}
              />
              <Redirect to="/campaigns" />
            </Switch>
          </Layout>
        </Router>
      </CampaignsProvider>
    </SegmentsProvider>
  </ShopProvider>
);

export default App;
